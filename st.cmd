#
# Module: essioc
#
require essioc

#
# Module: cabtr
#
require cabtr


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${cabtr_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-TC-005
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_monitor.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TC-005, IPADDR = ts2-cabtr-05.tn.esss.lu.se, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-011
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-011, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 1, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-061
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-061, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 2, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-063
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-063, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 3, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-019
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-019, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 4, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-001
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-001, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 5, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-002
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-002, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 6, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-003
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-003, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 7, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-028
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-028, CONTROLLER = TS2-010CRM:Cryo-TC-005, CHANNEL = 8, POLL = 1000")
